%% Initialize workspace
clear; format short e;
figure(1); clf; figure(2); clf

%% Load LabVIEW Data
load speakerdata.csv

%Load variables
time = speakerdata(:,1);
ch2=speakerdata(:,2);
ch1=speakerdata(:,3);
SR=10000;
Ts = 1/SR;
Trun = 10;
Npts = floor(Trun / Ts);

%% Generate TF estimate
[EstAccTF,EstAccF] = tfestimate(ch2,ch1,[],[],[],SR);
%% Remove 0 frequency due to division later...
EstAccTF = EstAccTF(2:end);
EstAccF  = EstAccF(2:end);
%% Split into component parts
EstAccMag   = abs(EstAccTF);
EstAccPhase = angle(EstAccTF);
EstAccOmega = EstAccF*2*pi;

%% Generate Plots
% Time Domain plot
figure(1);
INC = floor(Npts/1000);
plot(time(1:INC:end),ch1(1:INC:end),...
     time(1:INC:end),ch2(1:INC:end));
xlabel('Time (s)')
ylabel('Measured Voltage (V)')
title('Time Domain Plot')
legend('Ch0','Ch1')
% Transfer Function plot
figure(2)
semilogx(EstAccOmega, (20*log10(EstAccMag)))
xlabel('Angular Frequency (rad/s)')
ylabel('|H| (dB)')
title('Transfer Function Estimate')

%% Note about saving data
fprintf('\n If you wish to save your data \n')
fprintf('\n Use ''save FILENAME ch1 ch2 time EstAccTF EstAccF''\n');
