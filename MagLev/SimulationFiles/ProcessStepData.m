% Use this script to load simulation values from LabVIEW
% and calculate step response data
% Edit as necessary

%% Initialize things
clear; format short e

%% Load Data
load stepdata.csv

%% Parse Data File
time       = stepdata(:,1);
setpoint   = stepdata(:,2);
measheight = stepdata(:,3);

%% Calculate Stuff
% Step Response
sys = stepinfo(measheight, time)
OS  = sys.Overshoot;           %This is how you access variables from stepinfo

%Damping
y = log(OS/100);               %Make zeta equation easier
zeta = (-y)/(sqrt(pi^2 + y^2)) %Damping Ratio

%% Make Plots
figure(1); clf
plot(time, setpoint, time, measheight)
xlabel('Time (s)')
ylabel('Distance from Equilibrium (mm)')
title('System Response (ebs27)')
legend('SP', 'PV')