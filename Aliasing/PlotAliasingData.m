% This program was created  by Monica Rivera on Sept. 20, 2007.
% Modified on January 10 2014 by Michael R. Gustafson II.
% Last modified on Jan 12 2022 by Eric Stach

%% Start from scratch
close all
clear all

%% Load the data file and sets variables
load datarun.csv %change to your filename
datafile = datarun; %change to your filename

%Assign variables
time = datafile(:,1);
ch1 = datafile(:,2);
ch2 = datafile(:,3);

%% Define constants
% Sets the number of Averages and the number of points in the fft and
% determines the number of data points to acquire.
Nave=10;
Nsamp=2^12;
Npts=Nsamp*Nave;
% set variables for sample rate and duration
SRdaq=20000;
Syy=zeros(Nsamp,1);
Syy2=zeros(Nsamp,1);

for m=1:Nave;
    % Splits the data into data sets.  If Nsamp=1024 the data sets would be
    % (1:1024, 1025:2024, etc)
    n=[1:Nsamp]+Nsamp*(m-1);
    
    % Measured output split into the data sets
    yout=ch1(n);
    yout2=ch2(n);
    
    % Fast Fourier Transform of the measured data
    Y=fft(yout,Nsamp);
    Y2=fft(yout2,Nsamp);
    
    % PSD of the Data
    Syy=Syy+conj(Y).*Y/(Nsamp^2);
    Syy2=Syy2+conj(Y2).*Y2/(Nsamp^2);
    
end;

% Averaged PSD
Syy=Syy/Nave;
Syy2=Syy2/Nave;

% Eliminates the redundant points
Syy=Syy(1:floor(Nsamp/2));
Syy2=Syy2(1:floor(Nsamp/2));

% Calculates the frequency range
freq=(0:Nsamp/2-1)*SRdaq/Nsamp;
df=freq(11)-freq(10);

% Power Spectrum
Syy=Syy/df;
Syy2=Syy2/df;

% Plots
figure(1)
plot(time(1:SRdaq/10),ch1(1:SRdaq/10),time(1:SRdaq/10),ch2(1:SRdaq/10));
xlabel('Time (s)')
ylabel('Measured Voltage (V)')
title('Time Domain Plot')
legend('Original', 'Filtered')

figure(2)
subplot(2,1,1)
loglog(freq,Syy)
xlabel('Frequency (Hz)')
title('Power Spectrum Original')

subplot(2,1,2)
loglog(freq,Syy2)
xlabel('Frequency (Hz)')
title('Power Spectrum Filtered')

figure(3)
loglog(freq,Syy,freq,Syy2)
xlabel('Frequency (Hz)')
title('Power Spectrum')
legend('Original', 'Filtered')
fprintf('\n DO NOT FORGET TO SAVE YOUR DATA!!! \n')