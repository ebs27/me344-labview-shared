% Template for Suspension Problem MATLAB Code
% Most of what is included in this template is the file handling stuff 
% Name/net ID:

%% INITIALIZE
clear; format short e

%% LOAD DATASET
load ADD_YOUR_FILE_HERE            % 2.5 V (~11 mm step)
datafile = ADD_YOUR_FILE_HERE;     % So you only have to change datafile once

%% HANDLE VARIABLES
% System Constants

% Assign Variables from Datafile
t_final     = 10000;                   % Final time(ms) - ADJUST AS NEEDED 
t           = datafile(1:t_final,1);   % time
road        = datafile(1:t_final,3);   % Road position - use as 'r' for lsim!
position1	= datafile(1:t_final,4);   % Top position

% Find initial value
roadzero     = datafile(1,3);       % Initial road position
pos1zero     = datafile(1,4);       % Initial top position

% Subtract Initial Value to Zero Measurement
road      = road - roadzero;        % Road zeroed
position1 = position1 - pos1zero;   % Top Plate Position Zeroed

%% GENERATE TRANSFER FUNCTION


%% GENERATE STEP RESPONSE PLOT


%% SINE INPUT PLOTS 


%% BODE PLOT
